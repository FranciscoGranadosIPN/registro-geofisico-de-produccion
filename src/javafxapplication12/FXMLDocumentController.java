package javafxapplication12;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

public class FXMLDocumentController implements Initializable, ChangeListener {

    private Label label;
    @FXML
    private TableView<DatosDelPozo> tabla1;
    @FXML
    private TableColumn<DatosDelPozo, String> comlumnaDeep, comlumnaD15, comlumnaD30, comlumnaD45, comlumnaU15, comlumnaU30, comlumnaU45;
    @FXML
    private Slider slider1, slider2, sliderUnoPanel3, sliderDosPanel3, sliderTresPanel3;
    @FXML
    public LineChart<Number, Number> linechart1;
    @FXML
    private Button boton1, boton2, BotonSelccionarCaso, BotonLimpiar, BotonCalculosdeVelocidad, CalculoAjusteSliders;
    @FXML
    private Pane panel1, panel2, panel3;
    @FXML
    private ComboBox<String> combo1;
    public ObservableList<DatosDelPozo> data;
    public ObservableList<String> lista = FXCollections.observableArrayList();
    public ObservableList<String> listaMetodos = FXCollections.observableArrayList("KJT", "DTT-A");

    public BaseDeDatos datosconex;
    public Double valorUno, valorDos, valorTres;
    public String p, profundidad, velMediaTabla, velFinalTabla;
    public double ValorUnoRegresionUp, ValorDosRegresionUp, ValorTresRegresionUp, ValorUnoRegresionDown, ValorDosRegresionDown, ValorTresRegresionDown;
    public double sumatoriaYDown, XporY, xcuadrado, regresionDown, bDown, sumatoriaYup, XporYup, regresionUp, bUp, vhp, vhn, vth, interY, vf, vfm;
    public double gastoCurtis, colgamiento, areatuberia, VelMedia, VelResb, gastoil, gastowater, area, qo, qw, qt;

    public ObservableList<XYChart.Series<Number, Number>> ScatterData = FXCollections.observableArrayList();
    public final XYChart.Series<Number, Number> series1 = new XYChart.Series<>();
    public final XYChart.Series<Number, Number> serie2 = new XYChart.Series<>();
    public final XYChart.Series<Number, Number> serie3 = new XYChart.Series<>();

    @FXML
    private TextField textProfundidad1, textProfundidad2, textSlider1, textSlider2;
    @FXML
    public AreaChart<String, Number> areachart;
    public ObservableList<ScatterChart.Series<String, Number>> AreaChartData = FXCollections.observableArrayList();
    public final AreaChart.Series<String, Number> series2 = new AreaChart.Series<>();
    public final AreaChart.Series<String, Number> series3 = new AreaChart.Series<>();
    @FXML
    private TableView<TablaGastos> Tabla2;
    @FXML
    private TableColumn<TablaGastos, String> velocidad1, velocidad2;
    public ObservableList<TablaGastos> datosVelocidad;
    @FXML
    private Tab TabCalculos;
    @FXML
    private Button botonBorrarGrafico;

    public double[] ProfundidadVector = new double[9];
    public double[] vectorUnoDown = new double[9];
    public double[] vectorDosDown = new double[9];
    public double[] vectorTresDown = new double[9];
    public double[] vectorUnoUp = new double[9];
    public double[] vectorDosUp = new double[9];
    public double[] vectorTresUp = new double[9];
    public double[] BDownGrafica = new double[9];
    public double[] BUpGrafica = new double[9];
    public int i = 1;
    public String profundidadchart;
    @FXML
    public ComboBox<String> ComboIntervalo1;
    public int x = 1;
    public int z = 1;
    @FXML
    private ComboBox<String> comboMetodo;
    @FXML
    private Button boton4;
    @FXML
    private Pane panel4;
    @FXML
    private Button BotonAjustarProduccion;
    @FXML
    public AreaChart<String, Number> areachartProduccion;
    public ObservableList<ScatterChart.Series<String, Number>> AreaChartDataProduccion = FXCollections.observableArrayList();
    public final AreaChart.Series<String, Number> series1Produccion = new AreaChart.Series<>();
    public final AreaChart.Series<String, Number> series2Produccion = new AreaChart.Series<>();
    public final AreaChart.Series<String, Number> series3Produccion = new AreaChart.Series<>();
    @FXML
    private TextField tfUnoPanel3, tfDosPanel3, tsTresPanel3;

    public double densidadAceite, densidadGas, densidadAgua, impedanciaOil, impedanciaAgua, impedanciaTool;
    @FXML
    private TextField tfOilImpadencia, tfAguaImpadencia, tfHerramientaImpadencia;
    @FXML
    private Slider SliderImpadenciaAceite, SliderImpadenciaAgua, SliderImpadenciaHerramienta;
    @FXML
    private AreaChart<Number, Number> areachartnumerosimpedancia;

    public double valueToolOneX[] = new double[]{0.0, 0.12, 0.19, 0.27, 0.40, 0.53, 0.62, 0.70, 0.80, 0.90};
    public double valueToolOneY[] = new double[]{0.0, 0.17, 0.35, 0.54, 0.69, 0.79, 0.83, 0.88, 0.93, 0.98};
    public double valueToolTwoX[] = new double[]{0.0, 0.12, 0.19, 0.27, 0.40, 0.53, 0.62, 0.70, 0.80, 0.80};
    public double valueToolTwoY[] = new double[]{0.0, 0.17, 0.32, 0.54, 0.71, 0.79, 0.87, 0.88, 0.95, 0.99};
    public String output;
    public double frecuenciaLeida;
    @FXML
    private TextField TextFieldTreshPositivo;
    @FXML
    private TextField TextFieldSlopePositive;
    @FXML
    private TextField TextFieldTreshNegative;
    @FXML
    private TextField TextFieldSlopeNegative;
    
    
      public int ProfundidadAjuste;
      public int ObtenciondeIntervalo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        slider1.valueProperty().addListener(this);
        slider2.valueProperty().addListener(this);
        sliderUnoPanel3.valueProperty().addListener(this);
        sliderDosPanel3.valueProperty().addListener(this);
        sliderTresPanel3.valueProperty().addListener(this);
        SliderImpadenciaAceite.valueProperty().addListener(this);
        SliderImpadenciaAgua.valueProperty().addListener(this);
        SliderImpadenciaHerramienta.valueProperty().addListener(this);
        comboMetodo.setItems(listaMetodos);
        datosconex = new BaseDeDatos();

        valorTres = 0.0;

    }

    @FXML
    private void LoadDatafromBD(ActionEvent event) {
        if (event.getSource() == boton1) {

            panel1.toFront();
        } else if (event.getSource() == boton2) {

            panel2.toFront();
        } else if (event.getSource() == BotonCalculosdeVelocidad) {

            panel3.toFront();
        } else if (event.getSource() == boton4) {
            panel4.toFront();
        }
    }

    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
        valorUno = slider1.getValue();
        textSlider1.setText(String.valueOf(valorUno));
        valorDos = slider2.getValue();
        textSlider2.setText(String.valueOf(valorDos));

        densidadAceite = sliderUnoPanel3.getValue();
        tfUnoPanel3.setText(String.format("%.3f", densidadAceite));
        densidadAgua = sliderDosPanel3.getValue();
        tfDosPanel3.setText(String.format("%.3f", densidadAgua));
        densidadGas = sliderTresPanel3.getValue();
        tsTresPanel3.setText(String.format("%.3f", densidadGas));

        impedanciaOil = SliderImpadenciaAceite.getValue();
        tfOilImpadencia.setText(String.format("%.3f", impedanciaOil));
        impedanciaAgua = SliderImpadenciaAgua.getValue();
        tfAguaImpadencia.setText(String.format("%.3f", impedanciaAgua));
        impedanciaTool = SliderImpadenciaHerramienta.getValue();
        tfHerramientaImpadencia.setText(String.format("%.3f", impedanciaTool));

        valorTres = valorDos + valorUno;

        frecuenciaLeida = (impedanciaOil - impedanciaTool) / (impedanciaOil - impedanciaAgua);
        

    }

    @FXML

/////////////////////////////////////////////////////////////
    private void Clicked(ActionEvent event) {
        if (x > 1) {
            lista.clear();
        }
        x = x + 1;
        MostrarDatos();
    }

    public void MostrarDatos() {

        String valor1 = textProfundidad1.getText();
        String valor2 = textProfundidad2.getText();
        try {

            Connection cn = datosconex.Conexion();
            data = FXCollections.observableArrayList();
            datosVelocidad = FXCollections.observableArrayList();
            ResultSet rs = cn.createStatement().executeQuery("SELECT * FROM produccion WHERE Estacion between '" + valor1 + "' and '" + valor2 + "'");
            while (rs.next()) {
                data.add(new DatosDelPozo(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                //profundidad = rs.getString(2);
                lista.add(rs.getString(2));

                ProfundidadVector[i] = rs.getDouble(2);
                vectorUnoDown[i] = rs.getDouble(3);
                vectorDosDown[i] = rs.getDouble(4);
                vectorTresDown[i] = rs.getDouble(5);
                vectorUnoUp[i] = rs.getDouble(6);
                vectorDosUp[i] = rs.getDouble(7);
                vectorTresUp[i] = rs.getDouble(8);

                profundidadchart = String.valueOf(ProfundidadVector[i]);
                ValorUnoRegresionDown = vectorUnoDown[i];
                ValorDosRegresionDown = vectorDosDown[i];
                ValorTresRegresionDown = vectorTresDown[i];
                ValorUnoRegresionUp = vectorUnoUp[i];
                ValorDosRegresionUp = vectorDosUp[i];
                ValorTresRegresionUp = vectorTresUp[i];

                Regresion();
                CalculoVelocidades();
                CalculosGastosConvencional();
                CalculosGastosCurtis();
                i = i + 1;
            }

        } catch (SQLException ex) {
            System.err.println("error" + ex);
        }
        ComboIntervalo1.setItems(lista);
        comlumnaDeep.setCellValueFactory(new PropertyValueFactory<>("deep"));
        comlumnaD15.setCellValueFactory(new PropertyValueFactory<>("D15"));
        comlumnaD30.setCellValueFactory(new PropertyValueFactory<>("D30"));
        comlumnaD45.setCellValueFactory(new PropertyValueFactory<>("D45"));
        comlumnaU15.setCellValueFactory(new PropertyValueFactory<>("U15"));
        comlumnaU30.setCellValueFactory(new PropertyValueFactory<>("U30"));
        comlumnaU45.setCellValueFactory(new PropertyValueFactory<>("U45"));
        velocidad1.setCellValueFactory(new PropertyValueFactory<>("velMedia"));
        velocidad2.setCellValueFactory(new PropertyValueFactory<>("velFinal"));

        tabla1.setItems(null);
        Tabla2.setItems(null);
        tabla1.setItems(data);
        Tabla2.setItems(datosVelocidad);
        System.out.println("EL CONTADOR ES " + i);

    }

    public void Regresion() {
        ///////// CALCULOS DE REGRESION DOWN
        sumatoriaYDown = ValorUnoRegresionDown + ValorDosRegresionDown + ValorTresRegresionDown;
        XporY = (15 * ValorUnoRegresionDown) + (30 * ValorDosRegresionDown) + (45 * ValorTresRegresionDown);
        xcuadrado = 3150.00;
        regresionDown = ((3 * XporY) - (90 * sumatoriaYDown)) / ((3 * xcuadrado) - (8100));
        bDown = (sumatoriaYDown / 3) - (regresionDown * 30);
        ///////// CALCULOS DE REGRESION UP
        sumatoriaYup = ValorUnoRegresionUp + ValorDosRegresionUp + ValorTresRegresionUp;
        XporYup = ((0 - 15) * ValorUnoRegresionUp) + ((0 - 30) * ValorDosRegresionUp) + ((0 - 45) * ValorTresRegresionUp);
        regresionUp = ((3 * XporYup) - ((0 - 90) * sumatoriaYup)) / ((3 * xcuadrado) - (8100));
        bUp = (sumatoriaYup / 3) - (regresionUp * (0 - 30));

        BDownGrafica[i] = bDown;
        BUpGrafica[i] = bUp;

    }

    public void CalculoVelocidades() {
        vhp = (bDown * (-1)) / (regresionDown);
        vhn = (bUp * (-1)) / (regresionUp);
        vth = ((vhp - vhn) / 2);
        interY = (ValorDosRegresionDown - ((45) * (1)));
        vf = (bDown / regresionDown) + (vth);
        vfm = ((vf * (0.86)) + valorTres);

        ////
        velFinalTabla = String.valueOf(vf);
        velMediaTabla = String.valueOf(vfm);
        series2.getData().add(new AreaChart.Data<>(profundidadchart, vfm));
        series3.getData().add(new AreaChart.Data<>(profundidadchart, vf));
        AreaChartData.add(series2);
        AreaChartData.add(series3);
        areachart.setData(AreaChartData);

        datosVelocidad.add(new TablaGastos(velMediaTabla, velFinalTabla));
    }
/////////////////////////////////////////////////////////////

    public void CalculosGastosConvencional() {
        area = 0.005518;
        qo = (area) * (vfm) * (9056.603774) * (1.3) * (1 - 0.08);
        qw = (area) * (vfm) * (9056.603774) * (1) * (0.08);
        qt = qo + qw;

    }

    public void CalculosGastosCurtis() {
        gastoCurtis = ((ValorTresRegresionDown) * (60.5111));
        colgamiento = (0.87 - 0.86) / (1 - 0.86);
        areatuberia = 0.1431;
        VelMedia = ((gastoCurtis * (0.003899305556)) / areatuberia);
        VelResb = ((9.78) / (areatuberia * (1 - colgamiento))) - ((0.8494) / (areatuberia * colgamiento));
        gastoil = (areatuberia * (1 - colgamiento) * ((VelResb * areatuberia) + VelMedia)) * (1440 / 5.615);
        gastowater = gastoCurtis - gastoil;

    }

    private void ajustar(ActionEvent event) {

        int ProfundidadAjuste = Integer.valueOf(ComboIntervalo1.getSelectionModel().getSelectedItem());

        int ObtenciondeIntervalo = ((ProfundidadAjuste - 3250) / 10) + 1;

        //  for (i = ObtenciondeIntervalo; i <= ObtenciondeIntervalo2; i++) {
        series1.getData().add(new XYChart.Data<>((0 - 45), (vectorTresUp[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>((0 - 30), (vectorDosUp[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>((0 - 15), (vectorUnoUp[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>(15.0, (vectorUnoDown[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>(30.0, (vectorDosDown[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>(45.0, (vectorTresDown[ObtenciondeIntervalo])));

        serie2.getData().add(new XYChart.Data<>(BDownGrafica[ObtenciondeIntervalo], 0));
        serie2.getData().add(new XYChart.Data<>(45, (vectorTresDown[ObtenciondeIntervalo])));
        serie3.getData().add(new XYChart.Data<>((0 - 45), (vectorTresUp[ObtenciondeIntervalo])));
        serie3.getData().add(new XYChart.Data<>(BUpGrafica[ObtenciondeIntervalo], 0));

        ScatterData.add(series1);
        ScatterData.add(serie2);
        ScatterData.add(serie3);
        linechart1.getStylesheets().add(JavaFXApplication12.class.getResource("style.css").toExternalForm());
        
        linechart1.getStylesheets().add(JavaFXApplication12.class.getResource("style.css").toExternalForm());
        
        linechart1.setData(ScatterData);

        //  }
    }

    @FXML
    private void LimpiarDatos(ActionEvent event) {
        linechart1.getData().clear();
        serie3.getData().clear();
        serie2.getData().clear();
        series1.getData().clear();
    }

    @FXML
    private void TabclickCalculosVelocidad(Event event) {
    }

    @FXML
    private void ajustarSliders(ActionEvent event) {
        // MostrarDatos();

        datosVelocidad = FXCollections.observableArrayList();
        for (i = 1; i <= 6; i++) {
            profundidadchart = String.valueOf(ProfundidadVector[i]);
            ValorUnoRegresionDown = vectorUnoDown[i];
            ValorDosRegresionDown = vectorDosDown[i];
            ValorTresRegresionDown = vectorTresDown[i];
            ValorUnoRegresionUp = vectorUnoUp[i];
            ValorDosRegresionUp = vectorDosUp[i];
            ValorTresRegresionUp = vectorTresUp[i];

            Regresion();
            CalculoVelocidades();
            CalculosGastosConvencional();

            ///
        }
    }

    @FXML
    private void borrarGrafico(ActionEvent event) {
        series2.getData().clear();
        series3.getData().clear();
        areachart.getData().clear();
    }

    @FXML
    private void AjustarProduccion(ActionEvent event) {

        for (i = 1; i <= 6; i++) {

            profundidadchart = String.valueOf(ProfundidadVector[i]);
            ValorTresRegresionDown = vectorTresDown[i];
            CalculosGastosCurtis();
            series1Produccion.getData().add(new AreaChart.Data<>(profundidadchart, gastoil));
            series2Produccion.getData().add(new AreaChart.Data<>(profundidadchart, gastowater));
            series3Produccion.getData().add(new AreaChart.Data<>(profundidadchart, gastoCurtis));
            AreaChartDataProduccion.add(series1Produccion);
            AreaChartDataProduccion.add(series2Produccion);
            AreaChartDataProduccion.add(series3Produccion);

            areachartProduccion.setData(AreaChartDataProduccion);

            ///
        }

    }

    @FXML
    private void cambiarGraficaHerramienta(ActionEvent event) {
        ///calculo inicial de impedancia 
        double impedanciaEjemplo = 0.45;
        if (x > 1) {
            areachartnumerosimpedancia.getData().clear();
        }

        x = x + 1;
        XYChart.Series serieImpedancia = new XYChart.Series();
        output = comboMetodo.getSelectionModel().getSelectedItem();

        if (output.equals("DTT-A")) {

            serieImpedancia.getData().addAll(new XYChart.Data<>(0.0, 0.0));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.12, 0.17));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.19, 0.35));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.27, 0.54));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.40, 0.69));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.53, 0.79));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.62, 0.83));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.70, 0.88));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.80, 0.93));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.90, 0.98));
            areachartnumerosimpedancia.getData().addAll(serieImpedancia);

            for (i = 0; i <= 10; i++) {
                if (frecuenciaLeida >= valueToolOneX[i] && frecuenciaLeida <= valueToolOneX[i + 1]) {
                    System.out.println("El vector numero uno esta entre " + valueToolOneX[i] + " y el vector " + valueToolOneX[i + 1]);
                }
            }

        } else if (output.equals("KJT")) {
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.0, 0.0));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.12, 0.17));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.19, 0.32));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.27, 0.54));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.40, 0.71));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.53, 0.79));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.62, 0.87));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.70, 0.88));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.80, 0.99));
            serieImpedancia.getData().addAll(new XYChart.Data<>(0.90, 0.99));
            areachartnumerosimpedancia.getData().addAll(serieImpedancia);

            for (i = 0; i <= 10; i++) {
                if (frecuenciaLeida > valueToolOneX[i] && frecuenciaLeida < valueToolOneX[i + 1]) {

                    System.out.println("El vector esta entre " + valueToolOneX[i] + " y el vector " + valueToolOneX[i + 1]);
                }
            }
        }

    }

    @FXML
    private void CambioDeajuste(ActionEvent event) {

     
        
        ProfundidadAjuste = Integer.valueOf(ComboIntervalo1.getSelectionModel().getSelectedItem());
        ObtenciondeIntervalo = ((ProfundidadAjuste - 3250) / 10) + 1;
        System.out.println(ProfundidadAjuste);
        System.out.println(ObtenciondeIntervalo);
        
        
        //  for (i = ObtenciondeIntervalo; i <= ObtenciondeIntervalo2; i++) {
        series1.getData().add(new XYChart.Data<>((0 - 45), (vectorTresUp[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>((0 - 30), (vectorDosUp[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>((0 - 15), (vectorUnoUp[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>(15.0, (vectorUnoDown[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>(30.0, (vectorDosDown[ObtenciondeIntervalo])));
        series1.getData().add(new XYChart.Data<>(45.0, (vectorTresDown[ObtenciondeIntervalo])));

        serie2.getData().add(new XYChart.Data<>(BDownGrafica[ObtenciondeIntervalo], 0));
        serie2.getData().add(new XYChart.Data<>(45, (vectorTresDown[ObtenciondeIntervalo])));
        serie3.getData().add(new XYChart.Data<>((0 - 45), (vectorTresUp[ObtenciondeIntervalo])));
        serie3.getData().add(new XYChart.Data<>(BUpGrafica[ObtenciondeIntervalo], 0));

        ScatterData.add(series1);

        linechart1.getStylesheets().add(JavaFXApplication12.class.getResource("style.css").toExternalForm());
        ScatterData.add(serie2);
        ScatterData.add(serie3);
        linechart1.setData(ScatterData);
        
        TextFieldTreshPositivo.setText(String.valueOf(regresionDown));
        TextFieldTreshNegative.setText(String.valueOf(regresionUp));
        TextFieldSlopePositive.setText(String.valueOf(bDown));
        TextFieldSlopeNegative.setText(String.valueOf(bUp));
        

    }

}
