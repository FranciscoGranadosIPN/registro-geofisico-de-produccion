package javafxapplication12;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;




public class TablaGastos {
    
    private final StringProperty velMedia;
    private final StringProperty velFinal;
    
    public TablaGastos(String velMedia, String velFinal){
        this.velFinal= new SimpleStringProperty(velFinal);
        this.velMedia = new SimpleStringProperty(velMedia);
    }
    public String getVelFinal(){
    return velFinal.get();
    }
    
    public String getVelMedia(){
    return velMedia.get();
    }
    
    public void setVelFinal(String value){
    velFinal.set(value);    
    }
    public void setVelMedia(String value){
    velMedia.set(value);    
    }
    
    
    public StringProperty velFinalProperty(){
    return velFinal;
    }
    public StringProperty velMediaProperty(){
    return velMedia;
    }
}
