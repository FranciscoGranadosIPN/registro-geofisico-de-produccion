package javafxapplication12;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DatosDelPozo {

    private final StringProperty deep;
    private final StringProperty D15;
    private final StringProperty D30;
    private final StringProperty D45;
    private final StringProperty U15;
    private final StringProperty U30;
    private final StringProperty U45;

    public DatosDelPozo(String deep, String D15, String D30, String D45, String U15, String U30, String U45) {
        this.deep = new SimpleStringProperty(deep);
        this.D15 = new SimpleStringProperty(D15);
        this.D30 = new SimpleStringProperty(D30);
        this.D45 = new SimpleStringProperty(D45);
        this.U15 = new SimpleStringProperty(U15);
        this.U30 = new SimpleStringProperty(U30);
        this.U45 = new SimpleStringProperty(U45);

    }
//getter

    public String getDeep() {
        return deep.get();
    }

    public String getD15() {
        return D15.get();
    }

    public String getD30() {
        return D30.get();
    }

    public String getD45() {
        return D45.get();
    }

    public String getU15() {
        return U15.get();
    }

    public String getU30() {
        return U30.get();
    }

    public String getU45() {
        return U45.get();
    }

//setter
    public void setDeep(String Value) {
        deep.set(Value);
    }

    public void setD15(String Value) {
        D15.set(Value);
    }

    public void setD30(String Value) {
        D30.set(Value);
    }

    public void setD45(String Value) {
        D45.set(Value);
    }

    public void setU15(String Value) {
        U15.set(Value);
    }

    public void setU30(String Value) {
        U30.set(Value);
    }

    public void setU45(String Value) {
        U45.set(Value);
    }

    //property Values
    public StringProperty deepProperty() {
        return deep;
    }

    public StringProperty D15Property() {
        return D15;
    }

    public StringProperty D30Property() {
        return D30;
    }

    public StringProperty D45Property() {
        return D45;
    }

    public StringProperty U15Property() {
        return U15;
    }

    public StringProperty U30Property() {
        return U30;
    }

    public StringProperty U45Property() {
        return U45;
    }

}
