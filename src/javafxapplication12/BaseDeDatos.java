package javafxapplication12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BaseDeDatos {

    public Connection Conexion() {

        try {
            Class.forName("com.mysql.jdbc.Driver");  
            Connection cn = DriverManager.getConnection("jdbc:mysql://localhost/plt", "root", "");
            return cn;
        } catch (ClassNotFoundException |SQLException ex) {
            Logger.getLogger(BaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
}
